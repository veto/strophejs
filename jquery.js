$(document).ready(function()
{
  var Chat = function()
  {
    var self = this;
    this.connection = false;
    this.jid = false;
    this.BOSH_SERVICE = 'http://merc.biz:7070/http-bind';
    this.init = function()
    {
      $(".login_btn").button();
      $(".logout_btn").button();
      $(".msg_btn").button();
      self.connection = new Strophe.Connection(this.BOSH_SERVICE);
      self.connection.xmlInput = this.log;
      self.connection.xmlOutput = this.log;
      $('#disconnect').bind('click', function(event)
      {
        event.preventDefault();
        self.connection.disconnect();
      });
      $('#connect').bind('click', function(event)
      {
        event.preventDefault();
        var button = $('#connect').get(0);
        if (button.value == 'connect')
        {
          button.value = 'disconnect';
          self.jid = $('#jid').get(0).value;
          //console.log($('#jid').get(0).value);
          self.connection.connect(
            $('#jid').get(0).value,
            $('#pass').get(0).value,
            self.events.onConnect
          );
        }
        else
        {
          button.value = 'connect';
          self.connection.disconnect();
        }
      });

      $("#msg_btn").on("click", function(e)
      {
        e.preventDefault();
        console.log("Try to send: " + $("#message").val());
        $('#msg_logs').append($("#message").val() + '<br/>');
        self.send_message('veto@merc.biz',$("#message").val());
        $("#message").val("");
        return false;
      });
    };
    this.out = function(message)
    {
      $('#log').append('<br />').append(
        message
      );
    };
    this.log = function(message)
    {
      console.log(message);
    };
    this.events = {
      "onConnect": function(status)
      {
        if (status == Strophe.Status.CONNECTING)
        {
          $('#log').html('');
          self.out('Connecting chat server...');
        }
        else if (status == Strophe.Status.CONNFAIL)
        {
          self.out('Failed to connect.');
          $('#connect').get(0).value = 'connect';
        }
        else if (status == Strophe.Status.DISCONNECTING)
        {
          self.out('Disconnecting from chat server...');
        }
        else if (status == Strophe.Status.DISCONNECTED)
        {
          self.out('Disconnected.');
          $('#connect').get(0).value = 'connect';
          $('#login_form').show();
        }
        else if (status == Strophe.Status.CONNECTED)
        {
          self.out('Connected.');
          self.out('ECHOBOT: Send a message to ' + self.connection.jid +' to talk to me.');
          self.connection.addHandler(function(msg)
          {
            return self.events.onMessage(msg);
          }, null, 'message', null, null, null);
          self.connection.send($pres().tree());
          //self.groupchat.join();

        }
      },
      "onMessage": function(msg)
      {
        if (jQuery(msg).attr("type") == "chat")
        {
          $('#msg_logs').append(jQuery(msg).find("body:first").text() + '<br/>');
          self.out("<strong>" + jQuery(msg).attr("from") + ": </strong>" + jQuery(msg).find("body:first").text());
        }
        return true;
      }
    };

    this.send_message = function(recipient, message)
    {
      var reply = $msg(
        {
          to: recipient,
          type: "chat"
        })
        .c("body")
        .t(message);
      console.log("SENDING MESSAGE: " + message);
      self.connection.send(reply.tree());
      //self.out("<strong>" + self.connection.jid + ": </strong>" + message);
    };

    // initialisation
    this.init();
  };
  window.chat = new Chat();
});
