window.onload = function () {
  window.chat = new Chat();
};

var Chat = function()
{
  var self = this;
  this.connection = false;
  this.jid = false;
  this.BOSH_SERVICE = 'http://merc.biz:7070/http-bind';


  this.init = function()
  {
    self.connection = new Strophe.Connection(this.BOSH_SERVICE);
    self.connection.xmlInput = this.log;
    self.connection.xmlOutput = this.log;
    self.connection.si_filetransfer.addFileHandler(this.receive_file);
    document.getElementById("connect").addEventListener("click",this.connect); 
    document.getElementById("disconnect").addEventListener("click",this.disconnect); 
    document.getElementById("msg_btn").addEventListener("click",this.send_msg); 
  };
  
  this.receive_file = function(e)
  {
    console.log('rrrrrrrrrrrrrr');
    console.log(e);
    console.log('rrrrrrrrrrrrrr');
  };

  this.send_file = function(e)
  {
    self.connection.si_filetransfer.send(to, sid, filename, size, mime, function (err)
    {
     if(err)
     {
      return console.log(err);
     }
   });
  };

  this.send_msg = function(e)
  {
    e.preventDefault();
    var msg = document.getElementById("message").value;
    self.add_to_msg_log(msg);
    self.send_message('veto@merc.biz',msg);
    document.getElementById("message").value = "";
    console.log(msg);
  };
  this.connect = function(e)
  {
    e.preventDefault();
    this.jid = document.getElementById("jid").value;
    this.pass = document.getElementById("pass").value;
    self.connection.connect(this.jid,this.pass,self.events.onConnect);
  };

  this.disconnect = function(e)
  {
    e.preventDefault();
    self.connection.disconnect();
  };

  this.events = 
  {
    "onConnect": function(status)
    {
      if (status == Strophe.Status.CONNECTING)
      {
        document.getElementById("log").value = '';
        self.out('Connecting to the chat server ...');
      }
      else if (status == Strophe.Status.CONNFAIL)
      {
        self.out('Failed to connect.');
      }
      else if (status == Strophe.Status.DISCONNECTING)
      {
        self.out('Disconnecting from chat server...');
      }
      else if (status == Strophe.Status.DISCONNECTED)
      {
        self.out('Disconnected...');
      }
      else if (status == Strophe.Status.CONNECTED)
      {
        self.out('Connected.');
        self.out('ECHOBOT: Send a message to ' + self.connection.jid +' to talk to me.');
        self.connection.addHandler(function(msg)
        {
          return self.events.onMessage(msg);
        }, null, 'message', null, null, null);
        self.connection.send($pres().tree());
      }
      else if (status == Strophe.Status.ATTACHED)
      {
        self.out('Attached...');
      }

    },
    "onMessage": function(msg)
    {
      console.log(msg['attributes']['type']['nodeValue']);
      if (msg['attributes']['type']['nodeValue'] == "chat")
      {
       if(msg['textContent']) 
       {
         self.add_to_msg_log(msg['textContent']);
         self.out("<strong>" + msg['attributes']['from']['nodeValue'] + ": </strong>" + msg['textContent']);
       }
     }
      return true;
    }
  };

  this.send_message = function(recipient, message)
  {
    var reply = $msg(
    {
      to: recipient,
      type: "chat"
    })
    .c("body")
    .t(message);
    console.log("SENDING MESSAGE: " + message);
    self.connection.send(reply.tree());
    self.out("<strong>" + self.connection.jid + ": </strong>" + message);                                                               };

  this.add_to_msg_log = function(msg)
  {
    var lines = msg.split('\n');
    for(var x=0;x < lines.length;x++)
    {
      var msg_logs = document.getElementById("msg_logs");
      var p        = document.createElement("p");
      var txt     = document.createTextNode(lines[x]);
      p.appendChild(txt);
      msg_logs.appendChild(p);
    }

    return true;
  };

  this.out = function(message)
  {
    document.getElementById("log").value = document.getElementById("log").value  + '\n' + message;
  };
  this.log = function(message)
  {
    //console.log(message);
    console.log(message);
  };
  this.init();
};  


